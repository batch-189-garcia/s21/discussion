console.log('hello-s21');

/*
	Mini activity: 
		Display the ff. student number in our console. send output screenshot



*/



	let variable1 = '2020-1923'
	let variable2 ='2020-1924' 
	let variable3 = '2020-1925'
	let variable4 = '2020-1926'
	let variable5 = '2020-1927'
	console.log(variable1);
	console.log(variable2);
	console.log(variable3);
	console.log(variable4);
	console.log(variable5);

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];




/*

	Arrays
		-used to store multiple related values in single variable.
		-declared using square brackets ([]) also known as "Array Literals"

		Syntax:
			let/const arrayName = [elementA, elementB ... elementN]

*/

let grades =[98.5, 94.3, 89.2, 90.1]

let computerBrands =["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

let mixedArray = [12, "Asus", null, undefined, {}] //not recommended

console.log(mixedArray);

// Each element of an array 
let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"
];

console.log(myTasks);

// we can also store the values of seperate variables in an array

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
// let citySample = ["Tokyo", "Manila", "Jakarta"];
console.log(cities);
// console.log(cities == citySample);


// Array Length Proprty
console.log(myTasks.length) //4
console.log(cities.length) //3

let blankArr =[];
console.log(blankArr.length) // result is 0 - the array is empty

let fullname = "Jamie Noble";
console.log(fullname.length) //11

myTasks.length = myTasks.length - 1;
console.log(myTasks.length)
console.log(myTasks)

cities.length--;
console.log(cities);


cities.length++;
console.log(cities);


// Can we delete a character in a string using .length property?


fullname.length = fullname.length - 1;
console.log(fullname.length);
fullname.length--;
console.log('a'+fullname);

let theBeatles = ["John", "Paul", "Ringo", "George"];
console.log(theBeatles.length)

theBeatles.length++
console.log(theBeatles.length)
console.log(theBeatles)

// array[i] = "new value"
theBeatles[4] = "Rupert";
console.log(theBeatles);

/*
Accessing Elements of an Array

	Syntax:
		arrayName[index]

*/

console.log(grades[0]);
console.log(computerBrands[3]);

console.log(grades[20]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

// 


let currentlaker = lakersLegends[2]
console.log(lakersLegends[2]);

console.log("Arrays before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Paul Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);

// Accessing the last elements of an array
let bullsLegend = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"]
let lastElementIndex = bullsLegend.length - 1;
console.log(bullsLegend[lastElementIndex]);

console.log(bullsLegend[bullsLegend.length - 1]);

// Adding Items into the Array
let newArr = []
console.log(newArr)
console.log(newArr[0])
newArr[0] = "Jennie";
console.log(newArr);
newArr[1] = "Jisoo";
console.log(newArr);

newArr[newArr.length] = "Lisa";
console.log(newArr);




